﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationKeeper : MonoBehaviour {

	public GameObject EnemyPrefab;
	public GameObject[] Enemies;

	public float Width = 10f;
	public float Height = 5f;
	public float MovementSpeed = 5;
	public int FormationShotsPerSecond = 3;

	private float xMin;
	private float xMax;

	private bool mutex = false;

	private enum MoveDirection {LEFT,RIGHT};
	private MoveDirection currentDirection = MoveDirection.RIGHT;

	// Use this for initialization
	void Start () {
//		Debug.Log ("Loaded formation: " + this.name);
		SetupPlayspaceBoundaries ();
		SetupEnemies ();
	}

	// Update is called once per frame
	void Update () {
		MoveFormation ();
		CheckViewportCollision ();

		if (!mutex) {
			CoordinateFire ();
		}
	}



	void SetupPlayspaceBoundaries() {
//		Debug.Log ("Setting up play space boundaries");

		float distanceToCamera = this.transform.position.z - Camera.main.transform.position.z;
		Vector3 cameraLeftEdge = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distanceToCamera));
		Vector3 cameraRightEdge = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distanceToCamera));
		xMin = cameraLeftEdge.x;
		xMax = cameraRightEdge.x;

//		Debug.Log ("Left screen edge = " + xMin);
//		Debug.Log ("Right screen edge = " + xMax);
	}

	void SetupEnemies() {
//		Debug.Log ("Setting up enemies in a formation");

		Position[] formationPositions = GetComponentsInChildren<Position> ();
		Enemies = new GameObject[formationPositions.Length];

		int i = 0;
		foreach (Position position in formationPositions) {
			Enemies[i++] = SpawnEnemy (position);
//			Debug.Log ("Spawned enemy @ position = " + position.transform.position);
		}
	}
		
	void OnDrawGizmos() {
//		Debug.Log ("Drawing formation boundary gizmos");

		Gizmos.DrawWireCube(this.transform.position,new Vector3(Width,Height,transform.position.z));
	}

	void CheckViewportCollision(){
//		Debug.Log ("Checking viewport collision");

		float leftEdgePosition = this.transform.position.x - (Width / 2f);
		float rightEdgePosition = this.transform.position.x + (Width / 2f);

		//if formation collides with the viewport's left or right boundary, change movement direction	
		if (leftEdgePosition <= xMin) {
			currentDirection = MoveDirection.RIGHT;
		} else if (rightEdgePosition >= xMax) {
			currentDirection = MoveDirection.LEFT;
		}
	}

	void MoveFormation() {
		if (currentDirection == MoveDirection.RIGHT) {
//			Debug.Log ("Moving right");
			this.transform.position += Vector3.right * MovementSpeed * Time.deltaTime;
		} else {
//			Debug.Log ("Moving left");
			this.transform.position += Vector3.left * MovementSpeed * Time.deltaTime;
		}	
	}

	GameObject SpawnEnemy(Position position) {
//		Debug.Log ("Spawning enemy...");
		GameObject enemy = Instantiate (EnemyPrefab, position.transform.position, Quaternion.identity);
		enemy.transform.parent = position.transform;

		return enemy;
	}

	void CoordinateFire() {
		int shooterId = Random.Range (0, Enemies.Length - 1);

		if (Enemies [shooterId]) {
			Enemies [shooterId].GetComponent<Enemy>().FireWeapon();
		}

		StartCoroutine (StopWatch (1f / FormationShotsPerSecond));
	}

	IEnumerator StopWatch(float time) {
		mutex = true;
		yield return new WaitForSeconds(time);
		mutex = false;
	}
}
