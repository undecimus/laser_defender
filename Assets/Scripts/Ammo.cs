﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {

	public int Power = 1;

	private Weapon weapon;

	// Use this for initialization
	void Start () {
		this.weapon = this.transform.parent.gameObject.GetComponent<Weapon> ();
		this.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, weapon.GetVelocity());
	}

	public string GetSourceTag() {
		return this.weapon.tag;
	}

	public int GetDamage() {
		return Power;
	}

	public void Hit(){
		Destroy (gameObject);
	}

}
