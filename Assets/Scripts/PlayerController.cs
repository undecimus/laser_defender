﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public int Health = 3;
	public float MoveSpeed = 10;
	public float Padding = 0.5f;

	private float xMin;
	private float xMax;

	private Weapon weapon;
	private LevelManager levelManager;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager> ();

		float distance = this.transform.position.z - Camera.main.transform.position.z;

		Vector3 leftmost = Camera.main.ViewportToWorldPoint (new Vector3 (0,0,distance));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint (new Vector3 (1,0,distance));

		xMin = leftmost.x + Padding;
		xMax = rightmost.x - Padding;

		weapon = this.GetComponentInChildren<Weapon> ();
	}
	
	// Update is called once per frame
	void Update () {

		CalculateMovement ();

		if (Input.GetKeyDown(KeyCode.Space)) {
			weapon.StartFire ();

		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			weapon.StopFire ();
		}

	}

	private void CalculateMovement(){ 

		if (Input.GetKey (KeyCode.LeftArrow)) {
			this.transform.position += Vector3.left * MoveSpeed * Time.deltaTime;
		} 
		else if (Input.GetKey (KeyCode.RightArrow)) {
			this.transform.position += Vector3.right * MoveSpeed * Time.deltaTime;
		}

		float newX = Mathf.Clamp (this.transform.position.x, xMin, xMax);
		this.transform.position = new Vector3 (newX, this.transform.position.y, this.transform.position.z);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Debug.Log ("Player is hit!");
		Ammo missile = collider.gameObject.GetComponent<Ammo>();

		if (missile && missile.GetSourceTag() == "EnemyWeapon") {
			Health -= missile.GetDamage ();
			missile.Hit ();

			if (Health <= 0) {
				Destroy (gameObject);
				levelManager.LostLife ();
			}
		}
	}

}
