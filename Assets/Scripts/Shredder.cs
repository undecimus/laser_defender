﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shredder : MonoBehaviour {


	void OnCollisionEnter2D(Collision2D collision) {

		Ammo missile = collision.gameObject.GetComponent<Ammo> ();

		if (missile) {
			missile.Hit ();
		}

	}
}
