﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public int Health = 1;
	private Weapon weapon;

	void OnTriggerEnter2D(Collider2D collider) {
		Ammo missile = collider.gameObject.GetComponent<Ammo>();

		if (missile && missile.GetSourceTag() == "Weapon") {
			Health -= missile.GetDamage ();
			missile.Hit ();

			if (Health <= 0) {
				Destroy (gameObject);
			}
		}
	}

	void Start() {
		this.weapon = GetComponentInChildren<Weapon> ();
	}

	public void FireWeapon() {
		weapon.FireSingle();
	}
		
}
