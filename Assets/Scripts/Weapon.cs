﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	public float Velocity = 50f;

	//TODO: implement repeat rate behavior!!!
	public float RepeatRate = 0.2f;
	public GameObject Ammo;

	//TODO: implement overheat of the weapon
	public float OverheatTime = 3f;
	public float RecoilTime = 2f;

	private bool isOverheated = false;
	private float weaponOverheatTimer = 0f;
	private float recoilTimer = 0f;
	private float repeatTimer = 0f;

	void Start() {
	}

	public void Fire() {
		if ((Time.time - weaponOverheatTimer) >= OverheatTime) {
			Debug.Log ("Weapon overheated");
			Overheat();
		}

		if (!isOverheated) {
			Debug.Log ("Firing weapon");
			Instantiate (Ammo, transform.position, Quaternion.identity,this.transform);
			repeatTimer = Time.time;
		} else {
			if ((Time.time - recoilTimer) >= RecoilTime) {
				isOverheated = false;
				Debug.Log ("Weapon recoiled from overheating");
			}
		}
	}

	public void StartFire(){
		if (Time.time - repeatTimer >= RepeatRate) {
			InvokeRepeating ("Fire", 0.00001f, RepeatRate);
		}
		weaponOverheatTimer = Time.time;
	}

	public void StopFire(){
		CancelInvoke ("Fire");
		weaponOverheatTimer = Time.time;
	}

	void OnDrawGizmos() {
		Gizmos.DrawWireCube (this.transform.position, Vector3.one);
	}

	public void Overheat() {
		isOverheated = true;
		recoilTimer = Time.time;
		StopFire ();
	}

	public float GetVelocity(){
		return (this.tag == "EnemyWeapon") ?
			-Velocity : Velocity;
	}

	public void FireSingle() {
		weaponOverheatTimer = Time.time;
		this.Fire ();
	}
}

